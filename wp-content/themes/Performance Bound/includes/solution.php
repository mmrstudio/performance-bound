<?php echo do_shortcode('[ps2id id=services target=#services /]'); ?>
<div class="container solution"> 

<div class="col-sm-12">
    <h1 class="solution-title"> How we can help your workplace performance...</h1>
<?php $bronze_spon = get_field('banners','option');
 if($bronze_spon) {
foreach($bronze_spon as $banner) {
?>
  
    <div class="col-sm-4 col-xs-12 busness-sol"> 
    	<div><img src="<?php echo $banner['image'];?>"> </div>
    	<div class="service-title"> <?php echo $banner['title']; ?> </div>
    	<div class="service-desc"> <?php echo $banner['description']; ?> </div>
    </div>

<?php }
}
?>

	<div class="contact contact-solution col-xs-12"><?php echo do_shortcode('[ps2id url=#contact-panel offset=100] Contact Us[/ps2id]'); ?> </div>
</div>


</div>