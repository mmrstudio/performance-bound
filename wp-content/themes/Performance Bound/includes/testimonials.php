<div class="container text-panel">

  
    <div class="col-sm-12 testimonials">
    	
    	<?php $testimonials = get_field('testimonials','option'); 

    	foreach($testimonials as $test) {
    		echo "<div class='testimonial-slide'>";
    		echo "<h1>";
    		echo $test['description'];
    		echo "</h1>";
    		echo "<hr/>";
    		echo "<div class='client-info'>" .$test['client_name'].", ". $test['business_name'] ."  </div>";
    		echo "</div>";
    		
    		$repeter = get_sub_field('repeter','option');
    		print_r('$repeter');
    		foreach($repeter as $r1) {
    			echo "yes";
    		}
    	} ?>
  
     </div>
     
</div>

<script>
  jQuery(document).ready(function($) {
    $('.testimonials').slick({
   
	      slidesToShow: 1,
	      autoplay: true,
	      autoplaySpeed: 6000,
	      infinite: true,
	      arrows:false,
	      dots:false

 	});
 });
</script>