<?php echo do_shortcode('[ps2id id="case-study" /]');?>
<div class="container case-studies">
	<div class=" ">
  <?php $items = get_field('items','option');?>
  
 <?php foreach($items as $item) 
  { 
  	$rounds = $item['sub_items']; 
  	?>
  	<div class="items">
  	<div class="col-lg-5 col-sm-6 col-xs-12"> <img src="<?php echo $item['image'];?>" > </div>
  	<div class="col-lg-7 col-sm-6 col-xs-12">
  		<div class="case_title"><?php echo $item['name']; ?> </div>
  		<div class="case">Case Study </div>
	  	
	  	<?php foreach($rounds as  $round) {  ?>

	  	 	<div class="case_name <?php echo $round['title'];?>"> <?php echo $round['title'];?> </div>
	  	 	<div class="case_desc"> <?php echo $round['description'];?> </div>

	  	<?php }  ?>

	  </div>
  	</div>

<?php   } ?>



 </div>
</div>