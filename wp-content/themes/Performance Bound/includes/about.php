<?php echo do_shortcode('[ps2id id="about" /]');?>
<div class="container about">

  
    <div class="col-sm-12 col-md-5 col-xs-12 left">
     <h1>Performance Bound will help you increase clarity & confidence within your workplace </h1>

 
   
     </div>
     <div class="col-sm-12 col-md-7 col-xs-12 right">
<?php echo get_field('about_text','option');?>
      </div>
  
</div>