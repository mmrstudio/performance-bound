	<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
?>
	<!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title><?php wp_title( '&#124;', true, 'right' ); ?></title>

		<?php wp_head(); ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
		
	
	</head>

<body <?php body_class(); ?>>

 

<div class="top-header" > 
	
<!-- Nav Wrap START -->
<div id="nav-container" >
	<div class="container">
	<div class="stickey-logo"> <a href="<?php echo home_url( '/' ); ?>"> <img src="<?php echo get_stylesheet_directory_uri();?>/core/images/sticky_logo.svg"></a></div>

			<?php wp_nav_menu( array(
					'container'       => 'div',
					'container_class' => 'main-nav',
					'theme_location'  => 'top-menu',
					'menu'  => 'Top Menu'
				)
			);

			?>
	
		<div class="contact-header"><button id="contact-us"> Contact Us</button> </div>
			
	 
			</div>
		</div>
	 </div>
</div>
	
</div>

 
<!-- Nav Wrap END -->

